package com.its.assembly.config;

import com.its.assembly.Properties.AppletProperties;
import com.its.assembly.service.AppletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author huguangjun
 * @className AppletsConfiguration
 * @date 2021/7/9
 */
@Configuration
@ConditionalOnWebApplication // web应用情况下才生效
@EnableConfigurationProperties(AppletProperties.class)
public class AppletConfiguration {

    @Autowired
    private AppletProperties appletProperties;

    @Bean
    public AppletService appletService(){
        AppletService appletService = new AppletService();
        appletService.setAppletProperties(appletProperties);
        return appletService;
    }
}
